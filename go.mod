module gitlab.com/shapeblock-buildpacks/nodejs

go 1.16

require (
	github.com/paketo-buildpacks/packit v0.13.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
