package node

import (
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type BuildPlanMetadata struct {
	Build   bool   `toml:"build"`
	Launch  bool   `toml:"launch"`
	Version string `toml:"version"`
}

type PlatformConfig struct {
	Name string                 `yaml:"name"`
	Type string                 `yaml:"type"`
	X    map[string]interface{} `yaml:"-"`
}

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.DetectResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.DetectResult{}, err
		}

		res := strings.Split(config.Type, ":")
		stack, version := res[0], res[1]

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: stack},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name: stack,
						Metadata: map[string]string{
							"version": version,
						},
					},
					{
						Name: stack,
					},
				},
			},
		}, nil
	}
}
