package node

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/paketo-buildpacks/packit"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {
		nodejsVersions := make(map[string]string)
		nodejsVersions["12"] = "https://nodejs.org/download/release/v12.22.1/node-v12.22.1-linux-x64.tar.gz"
		nodejsVersions["14"] = "https://nodejs.org/dist/v14.17.1/node-v14.17.1-linux-x64.tar.gz"
		nodejsVersions["16"] = "https://nodejs.org/dist/v16.4.0/node-v16.4.0-linux-x64.tar.gz"

		// fix caching
		entry := context.Plan.Entries[0]
		version, _ := entry.Metadata["version"].(string)
		uri := nodejsVersions[version]

		nodeLayer, err := context.Layers.Get("nodejs")
		if err != nil {
			return packit.BuildResult{}, err
		}
		versionFile := filepath.Join(nodeLayer.Path, "version")

		// if version file exists in node layer, return
		if fileExists(versionFile) {
			currentVersion, err := ioutil.ReadFile(versionFile)
			if err != nil {
				return packit.BuildResult{}, err
			}

			if string(currentVersion) == version {
				return packit.BuildResult{
					Plan: context.Plan,
					Layers: []packit.Layer{
						nodeLayer,
					},
				}, nil
			}
		}

		fmt.Printf("URI -> %s\n", uri)
		nodeLayer, err = nodeLayer.Reset()
		if err != nil {
			return packit.BuildResult{}, err
		}

		downloadDir, err := ioutil.TempDir("", "downloadDir")
		if err != nil {
			return packit.BuildResult{}, err
		}
		defer os.RemoveAll(downloadDir)

		fmt.Println("Downloading dependency...")
		err = exec.Command("curl",
			uri,
			"-o", filepath.Join(downloadDir, "node.tar.xz"),
		).Run()
		if err != nil {
			return packit.BuildResult{}, err
		}

		fmt.Println("Untaring dependency...")
		err = exec.Command("tar",
			"-xf",
			filepath.Join(downloadDir, "node.tar.xz"),
			"--strip-components=1",
			"-C", nodeLayer.Path,
		).Run()
		if err != nil {
			return packit.BuildResult{}, err
		}

		nodeLayer.Build = true
		nodeLayer.Launch = true
		nodeLayer.Cache = true
		fmt.Printf("Layer path -> %s\n", nodeLayer.Path)
		nodeLayer.SharedEnv.Default("NODE_HOME", nodeLayer.Path)
		nodeLayer.SharedEnv.Append("PATH", filepath.Join(nodeLayer.Path, "bin"), string(os.PathListSeparator))

		// Add node version file
		err = ioutil.WriteFile(versionFile, []byte(version), 0644)
		if err != nil {
			return packit.BuildResult{}, err
		}

		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				nodeLayer,
			},
		}, nil
	}
}
