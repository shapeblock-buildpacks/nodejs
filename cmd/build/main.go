package main

import (
	"gitlab.com/shapeblock-buildpacks/nodejs/node"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Build(node.Build())
}
